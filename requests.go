package kiv_lib

type GetMessagesReq struct {
	Recipient string
}

type SendMessageReq struct {
	Recipient string
	Requester string
	Message   string
}
