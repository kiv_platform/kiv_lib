package kiv_lib

import (
	"database/sql"
	"errors"
	"fmt"
)

// Message - Structure representing a message transmittet between users
type Message struct {
	Id        int
	Requester string
	Recipient string
	Message   string
}

func (m *Message) ToString() string {
	return fmt.Sprintf("Message(id=%d, requester=%s, recipient=%s, message=%s)", m.Id, m.Requester, m.Recipient, m.Message)
}

func (m *Message) Insert(database *sql.DB) error {
	if database == nil {
		return errors.New("kiv_lib: database is nil")
	}

	sqlStatemant := `
		INSERT INTO messages (requester, recipient, message)
		VALUES ($1, $2, $3)
		Returning id`

	return database.QueryRow(sqlStatemant, m.Requester, m.Recipient, m.Message).Scan(&m.Id)
}

func GetByRecipient(database *sql.DB, recipient string) ([]Message, error) {
	// Get all messages
	sqlStatement := `
		SELECT m.*
		FROM messages m
		WHERE m.Recipient = $1`

	rows, err := database.Query(sqlStatement, recipient)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	// Extract messages from rows
	var mssgs []Message
	for rows.Next() {
		var mssg Message
		err = rows.Scan(&mssg.Id, &mssg.Recipient, &mssg.Requester, &mssg.Message)
		if err != nil {
			return nil, err
		}
		// Add to slice
		mssgs = append(mssgs, mssg)
	}

	// Handle all errors occured whiel iterating
	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return mssgs, nil
}
